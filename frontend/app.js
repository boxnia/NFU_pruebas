
'use strict';
var app = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngCookies', 'ui.map', 'ui.event', 'facebook','lazy-scroll']);//, 'twitterApp.services'
//'ui.bootstrap' -> necesario para la paginación

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider

                // Home
                .when("/", {templateUrl: "./frontend/modules/home/view/home.view.html", controller: "MainCtrl"})
                // Pages

                ///users 
                .when("/users", {templateUrl: "./frontend/modules/users/view/users.view.html", controller: "UsersCtrl"})
                .when("/users_games", {templateUrl: "./frontend/modules/users/view/users_games.view.html", controller: "UsersGamesCtrl"})

                ///users admin
                .when("/admin", {templateUrl: "./frontend/modules/users/view/admin.view.html", controller: "AdminCtrl"})
                .when("/admin_users_list", {templateUrl: "./frontend/modules/users/view/admin_users.view.html", controller: "listCtrl"})
                .when("/admin_users_create", {templateUrl: "./frontend/modules/users/view/create_user_admin.view.html", controller: "listCtrl"})
                .when('/edit_user/:nombre', {
                    title: 'Edit User',
                    templateUrl: './frontend/modules/users/view/users.view.html',
                    controller: 'EditCtrl',
                    resolve: {
                        user_bd: function (services, $route) {
                            var nombre = $route.current.params.nombre;
                            return services.get('users', 'load_data_users', nombre);
                        }
                    }
                })

                ///login
                .when("/login", {templateUrl: "./frontend/modules/login/view/login.view.html", controller: "LoginCtrl"})
                .when("/recovery", {templateUrl: "./frontend/modules/login/view/recovery.view.html", controller: "LoginCtrl"})
                .when("/recovery_pass", {templateUrl: "./frontend/modules/login/view/recovery_pass.view.html", controller: "LoginCtrl"})
                //installation
                .when("/install", {templateUrl: "./frontend/modules/installation/view/list_install.view.html", controller: "InstallCtrl"})


                .when('/install_details/:installID', {
                    title: 'Details Install',
                    templateUrl: './frontend/modules/installation/view/detail_install.view.html',
                    controller: 'DetailsInstallCtrl',
                    resolve: {
                        install: function (services, $route) {

                            var installID = $route.current.params.installID;
                            console.log(installID);
                            return services.get('installation', 'idInstallation', installID);
                        }
                    }
                })

                //////games
                .when("/games_form", {templateUrl: "./frontend/modules/games/view/gamesform.view.html", controller: "GamesCtrl"})
                //////create install
                .when("/create_install", {templateUrl: "./frontend/modules/games/view/create_install.view.html", controller: "create_install"})
                ///contact
                .when("/contact", {templateUrl: "./frontend/modules/contact/view/contact.view.html", controller: "ContactCtrl"})
                .when('/logout',
                        {resolve: {redirect: function ($location,twitterService, authService, $cookies, Facebook) {
                                    authService.ClearCredentials('user');
                                    $cookies.remove('latitud');
                                    $cookies.remove('longitud');
                                    $cookies.remove('avatar');
                                    twitterService.clearCache();
                                    localStorage.removeItem("name");
                                    localStorage.removeItem("time");
                                    localStorage.removeItem("duration");
                                    localStorage.removeItem("places");
                                    localStorage.removeItem("day");
                                    localStorage.removeItem("zona");
                                    Facebook.logout(function () {

                                    });
                                    return $location.path("/");
                                    ;
                                }}})
                //


                // else main
                .otherwise({redirectTo: '/'});
    }]).config([
    'FacebookProvider',
    function (FacebookProvider) {
        var myAppId = '1757954731099648';

        // You can set appId with setApp method
        // FacebookProvider.setAppId('myAppId');

        /**
         * After setting appId you need to initialize the module.
         * You can pass the appId on the init method as a shortcut too.
         */
        FacebookProvider.init(myAppId);
    }
])



     
