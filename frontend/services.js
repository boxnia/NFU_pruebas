//////////////////////////////////////load menu and header
app.factory("FlashService", ['$rootScope', function ($rootScope,$cookies) {
        console.log("factoria flashservice")
        var service = {};
        service.load_menu = load_menu;
        service.load_header = load_header;
        return service;
        function load_menu(value) {
            //console.log(value)

            if (value.tipo === "normal") {

                $rootScope.user_l = true;
                $rootScope.user = false;
                $rootScope.user_l_a = false;
                //$rootScope.log($rootScope.menu.user)
            } else if (value.tipo === "admin") {
                $rootScope.user_l_a = true;
                $rootScope.user_l = true;
                $rootScope.user = false;
            } else {
                $rootScope.user_l_a = false;
                $rootScope.user_l = false;
                $rootScope.user = true;
            }

        }
        ;



        function load_header(value) {
            // console.log(value)
            ////control image facebook
            if(value.avatar===null){
                value.avatar=$cookies.get('avatar');
            }
            if (value.tipo == "normal") {
                $rootScope.avatar = value.avatar;
                $rootScope.headerMain = false;
                $rootScope.headerRegister = false;
                $rootScope.headerUsu = true;
                $rootScope.headerAdmin = false;

                //$rootScope.log($rootScope.menu.user)
            } else if (value.tipo == "admin") {
                $rootScope.avatar = value.avatar;
                $rootScope.headerMain = false;
                $rootScope.headerRegister = false;
                $rootScope.headerUsu = false;
                $rootScope.headerAdmin = true;
            } else if (value == "main") {
                $rootScope.headerMain = true;
                $rootScope.headerRegister = false;
                $rootScope.headerUsu = false;
                $rootScope.headerAdmin = false;

            } else {
                $rootScope.headerMain = false;
                $rootScope.headerRegister = true;
                $rootScope.headerUsu = false;
                $rootScope.headerAdmin = false;
            }

        }


    }]);
/////////////////////////////////$cookie
app.factory("authService", ['$cookies',
    function ($cookies) {

        var service = {};

        service.SetCredentials = SetCredentials;

        service.ClearCredentials = ClearCredentials;

        return service;



        function SetCredentials(user) {
          console.log(user);
            var user = JSON.stringify(user);
            var authdata = Base64.encode(user);
            var now = new Date(),
                    // this will set the expiration to 1 day
                    exp = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
            //console.log(exp);
            // $http.defaults.headers.common['Authorization'] = 'Basic ' + user.tipo;
            //  console.log('Basic ' + user.tipo)
            $cookies.putObject('user', authdata, {expires: exp});
        }






        function ClearCredentials() {
            // $rootScope.globals = {};
            $cookies.remove('user');
            //$http.defaults.headers.common.Authorization = 'Basic';
        }
    }]);

///////////////////////////////////////infinite scroll
app.factory('Scroll', function ($http) {



    var Scroll = function (lat, long) {
        //alert("hola2");
        this.lat = lat;
        this.long = long;
        this.items = [];
        this.busy = false;
        this.after = 1;
    };

    Scroll.prototype.nextPage = function () {
        // alert("hola");
        var this_ = this;
        if (this_.busy)
            return;
        this_.busy = true;
        var config = {
            method: "POST",
            url: "app_model/index.php?module=users&function=load_coordinates_near",
            data: {"lat_position": this_.lat,
                "long_position": this_.long,
                "p": this_.after, }
        }

        var response = $http(config);
        console.log(config);
        response.success(function (data, status, headers, config) {
            console.log(data);
            var items = data.game;

            //console.log(items);
            for (var i = 0; i < items.length; i++) {
                this_.items.push(items[i]);
                console.log(this_.items);
               

            }
            Scroll.after = this_.after++;
            this.busy = false;
        }.bind(this_));
    }


    return Scroll;
});






///////////////////////////////////////Facebook
app.factory('FacebookF', function (Facebook, $q) {

    var service = {};

    service.login = login;

    service.me = me;

    return service;


    function login() {
        var defered = $q.defer();
        var promise = defered.promise;
        promise = Facebook.login(function (response) {
            console.log(response);
            if (response.status == 'connected') {
                defered.resolve(true);

            } else {
                defered.resolve(false);
            }
         
        });
        
           return promise;
    }
    ;

    function me() {
        var defered = $q.defer();
        var promise = defered.promise;
        promise = Facebook.api("me?fields=name, id, email, picture", function (response) {
            console.log(response);
              defered.resolve(true);
            /**
             * Using $scope.$apply since this happens outside angular framework.
             */
            /*$scope.$apply(function () {
                $scope.user = response;
            });*/
        });
         return promise;
    }
    ;
    function logout() {

        Facebook.logout(function () {
            $scope.$apply(function () {
                $scope.user = {};
                $scope.logged = false;
            });
        });
    }
});

/*////////////////////////////////////////////////twitter*/

app.factory('twitterService', function($q) {
 
//console.log('linea 219 de services.js');
    var authorizationResult = false;

    return {
        initialize: function() {
            //initialize OAuth.io with public key of the application
            OAuth.initialize('1jcRzPojC5xmp5WFs7FyP8iFFa8', {cache:true});//publicKey que ofrece OAuth.io
            //try to create an authorization result when the page loads, this means a returning user won't have to click the twitter button again
            authorizationResult = OAuth.create('twitter');
              
        },
        isReady: function() {
            return (authorizationResult);
        },
        connectTwitter: function() {
            var deferred = $q.defer();
            OAuth.popup('twitter', {cache:true}, function(error, result) { //cache means to execute the callback if the tokens are already present
               console.log(result);
                if (!error) {
                    authorizationResult = result;
                    deferred.resolve();
                } else {
                    //do something if there's an error
                }
            });
            return deferred.promise;
        },
        clearCache: function() {
            OAuth.clearCache('twitter');
            authorizationResult = false;
        },
        getUserTwitter: function () {
            //create a deferred object using Angular's $q service
            var deferred = $q.defer();
            var promise = authorizationResult.get('/1.1/account/verify_credentials.json').done(function(data) { //https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
                //when the data is retrieved resolved the deferred object
                console.log(data)
                deferred.resolve(data);
            });
            //return the promise of the deferred object
            return deferred.promise;
        }
    }
    
});




























// Base64 encoding service used by AuthenticationService
var Base64 = {
    keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        do {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);
        return output;
    },
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        do {
            enc1 = this.keyStr.indexOf(input.charAt(i++));
            enc2 = this.keyStr.indexOf(input.charAt(i++));
            enc3 = this.keyStr.indexOf(input.charAt(i++));
            enc4 = this.keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);
        return output;
    }
};
