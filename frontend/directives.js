
//////////////////////////datepicker
app.directive('calendar', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).datepicker({
                dateFormat: 'mm-dd-yy',
                autoclose: true,
                enableYearToMonth: true,
                changeMonth: true,
                changeYear: true,
                onSelect: function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
})

/////////////////angular se utiliza camel case, en html hace referencia a ng-navside
//////////////////////////////pluguin menu
app.directive('ngNavside', function () {
    // console.log("navside")
    return{
        restric: 'A',
        link: function (scope, element, attrs) {
            $(element).navside(scope.$eval(attrs.ngNavside))
        }
    }
})
/////////////////////plugin carousel main
app.directive('ngCarousel', function () {
    // console.log("navside")
    return{
        restric: 'A',
        link: function (scope, element, attrs) {
            $(element).owlCarousel(scope.$eval(attrs.ngCarousel))
        }
    }
})


/////////////plugin las vegas main
app.directive('ngHeader', function () {
    // console.log("navside")
    return{
        restric: 'E',
        link: function (scope, element, attrs) {

            var h = window.innerHeight;



            $(element).vegas(scope.$eval(attrs.ngHeader))
        }
    }
})
/////////////////////////validar password
app.directive('passwordCheck', [function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                var firstPassword = '#' + attrs.passwordCheck;
                $(element).add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        // console.info(elem.val() === $(firstPassword).val());
                        ctrl.$setValidity('pwmatch', $(element).val() === $(firstPassword).val());
                    });
                });
            }
        }
    }]);
//////////////////////////upload_image
app.directive('dropzone', function () {
    return function (scope, element, attrs) {
        var config, dropzone;

        config = scope[attrs.dropzone];

        // create a Dropzone for the element with the given options
        dropzone = new Dropzone(element[0], config.options);

        // bind the given event handlers
        angular.forEach(config.eventHandlers, function (handler, event) {
            dropzone.on(event, handler);
        });
    };
});



/*////////////////////////////////////////////directve from time piker start*/
app.directive('timepiker_start', function () {
    return{
        //restrict:'A',
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).timepicker({
                //$(element).timepicker({
                timeFormat: 'h:i A',
                //scope.$eval(attrs.timepiker_start)
                onSelect: function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
});

/*////////////////////////////////////////////directve from time piker duration*/
app.directive('timepiker_duration', function () {
    return{
        //restrict:'A',
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).timepicker({
                timeFormat: 'H:i:s',
                onSelect: function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
});
/*////////////////////////////////////////////directve from time piker start*/
app.directive('timepiker_start', function () {
    return{
        //restrict:'A',
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).timepicker({
                //$(element).timepicker({
                timeFormat: 'h:i A',
                //scope.$eval(attrs.timepiker_start)
                onSelect: function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
});

/*////////////////////////////////////////////directve from time piker duration*/
app.directive('timepiker_duration', function () {
    return{
        //restrict:'A',
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).timepicker({
                timeFormat: 'H:i:s',
                onSelect: function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
});


/*////////////////////////////////////////////directve from calendar other format date that calendar*/
app.directive('calendario', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).datepicker({
                dateFormat: 'mm/dd/yy',
                autoclose: true,
                enableYearToMonth: true,
                changeMonth: true,
                changeYear: true,
                onSelect: function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
})